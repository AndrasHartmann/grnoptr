# GRNOpt package for R
Gene Regulatory Network Inference Using Optimization

GRNOpt infers Gene Regulatory Network (GRN) from booleanized transcriptomic data and Prior Knowledge Network (PKN) using optmization methods. The package also includes methods for simulation and visualization.



## Installation manual 

### Dependencies
- R (>=3.4)
- gurobi r package (>=8.0.0)

To install gurobi, please follow the instructions at: http://www.gurobi.com/documentation/8.0/

To clone the repository you can use e.g.

```bash
$ git clone ssh://git@git-r3lab-server.uni.lu:8022/andras.hartmann/GRNOptR.git
```

Then you can install the package from the source.

```R
> install.packages(path_to_file, repos = NULL, type="source")
```