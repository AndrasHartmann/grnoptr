# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.5.2] - 2018-08-01 (Reprogramming)
### Changed
- Updated documentation
### Added
- Functions for perturbation
	- Discover TFs that induce perturbation 
	- Testing a perturbation
- Load differential expression data
- Load function for differentially expressed data
- Simulation with adjacency matrix

## [0.5.1]
### Changed
- Bugfix when results are of zero length
- Improved package documentation

## [0.5]
### Changed
- Updates to support gurobi version to 8.0

## [0.4.1] - 2018-05-14
### Changed
- rgurobi is only suggested, not a dependency

### Added
- Dynamic simulation
- Recontextualization

## [0.4] - 2018-05-04
### Changed
- Removed two_attractors: deprecated flag
- seed for network inference for reproducibility

## [0.3.2] - 2018-02-06
### Changed
- Pruning returns only functional edges as default

### Added
- Conversion tools for SIF network format
- Support for network visualization
- verbose mode for pruning

## [0.3.1] - 2018-01-12
### Added
- Support for "Unspecified" edges

## [0.3.0] - 2018-01-08
### Added
- Functions to simulate the network
- Vignette for toy network

### Changed
- Imprved documentation

## [0.2.0]
### Features
- Correct returning multiple Inhibition Dominant (ID) solutions

### Added
- el2adj: to convert edge list to adjacency matrix
- writeNetwork: to write network to file
